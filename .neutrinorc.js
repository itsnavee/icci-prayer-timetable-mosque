module.exports = {
  use: [
    // '@neutrinojs/airbnb',
    // '@neutrinojs/airbnb-base',
    // '@neutrinojs/standardjs',
    // ['@neutrinojs/airbnb-base', {
    //   eslint: {
    //     rules: {
    //       'semi': 'off',
    //       'max-len': ['error', { 'code': 180 }]
    //     }
    //   }
    // }],
    [
      '@neutrinojs/airbnb',
      {
        eslint: {
          rules: {
            semi: 'off',
            'max-len': ['error', { code: 120 }],
            'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
            quotes: [2, 'single', { allowTemplateLiterals: true }],
            'no-console': ['error', { allow: ['warn', 'error', 'log'] }],
            'react/forbid-prop-types': 0,
            'no-underscore-dangle': [2, { allowAfterThis: true }],
            'class-methods-use-this': [
              'error',
              {
                exceptMethods: [
                  'appendZero',
                  'render',
                  'componentWillReceiveProps',
                  'componentWillMount',
                  'componentDidMount',
                  'componentWillUnmount',
                  '_directionCalc',
                  '_precisionRoundCalc',
                  '_qiblaDirectionCalc',
                  'handleConnectivityChange',
                  'online',
                  'snack',
                  '_getLocality',
                  'requestLocationPermission',
                  'titleCase',
                  'citySelect',
                ],
              },
            ],

            'prefer-destructuring': [
              'error',
              {
                array: false,
                object: false,
              },
              {
                enforceForRenamedProperties: false,
              },
            ],

            'react/no-array-index-key': 0,
            'no-lonely-if': 0,
            'no-mixed-operators': 0,
            'no-restricted-properties': 0,
            'import/no-unresolved': [2, { ignore: ['.png$', '.webp$', '.jpg$'] }],
            'react/require-default-props': 0,
            'no-return-assign': 0,
            'space-before-function-paren': 0,
            quotes: ['error', 'single', { avoidEscape: false }],
            'jsx-quotes': ['off', 'prefer-single'],
            'comma-dangle': ['error', 'always-multiline'],
            'global-require': 0,
            'import/no-dynamic-require': 0,
            'no-restricted-syntax': 0,
            'object-curly-newline': 0,
            'no-prototype-builtins': 0,
            'arrow-parens': 0,
            'function-paren-newline': 0,
          },
        },
      },
    ],
    [
      '@neutrinojs/react',
      {
        html: {
          title: 'prayer-timetable-react',
        },
      },
    ],
    '@neutrinojs/jest',
  ],
}
