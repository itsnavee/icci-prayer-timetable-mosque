import React, { Component } from 'react'
import PropTypes from 'prop-types'

import moment from 'moment-hijri'
import mainLogo from '../style/img/logo.svg'

class Clock extends Component {
  constructor(props) {
    super(props)

    this.state = {
      date: new Date(),
      day: this.props.day,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.date !== this.state.date) {
      this.setState({ date: nextProps.date })
    }
    if (nextProps.day !== this.state.day) {
      this.setState({ day: nextProps.day })
    }
  }

  render() {
    return (
      <div className="Clock">
        <img src={mainLogo} className="logo" alt="logo" />
        <div className="timeRow">{moment().format('H:mm:ss')}</div>
        <div className="dateRow">
          <div>{this.state.day.gregorian}</div>
          <div>{this.state.day.hijri}</div>
        </div>
      </div>
    )
  }
}

Clock.propTypes = {
  date: PropTypes.func,
  day: PropTypes.object,
}

export default Clock
