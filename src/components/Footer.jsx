import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment-hijri'
import { Offline, Online } from 'react-detect-offline'

import defsettings from '../settings.json'
import wifiOn from '../style/img/wifiOn.svg'
import wifiOff from '../style/img/wifiOff.svg'

class Footer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      settings: defsettings,
      day: {},
      refreshed: this.props.refreshed,
      jummuahTime: this.props.jummuahTime,
      taraweehTime: this.props.taraweehTime,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.settings !== this.state.settings && nextProps.settings !== null) {
      this.setState({ settings: nextProps.settings })
    }
    if (nextProps.day !== this.state.day && nextProps.day !== null) {
      this.setState({ day: nextProps.day })
    }
    if (nextProps.refreshed !== this.state.refreshed && nextProps.refreshed !== null) {
      this.setState({ refreshed: nextProps.refreshed })
    }
    if (nextProps.jummuahTime !== this.state.jummuahTime && nextProps.jummuahTime !== null) {
      this.setState({ jummuahTime: nextProps.jummuahTime })
    }
    if (nextProps.taraweehTime !== this.state.taraweehTime && nextProps.taraweehTime !== null) {
      this.setState({ taraweehTime: nextProps.taraweehTime })
    }
  }

  render() {
    let updated
    if (this.state.settings.updated < 1514764800) updated = '-'
    // 1/1/2018 0:00
    else updated = moment(this.state.settings.updated * 1000).format('DD/MM/YY')

    let ramadan
    if (this.state.day.ramadanCountdown) {
      ramadan = <div className="left">{this.state.day.ramadanCountdown} to Ramadan</div>
    }
    const jummuah = (
      <div className="left">
        {this.state.settings.labels.jummuah} {this.props.jummuahTime.format('H:mm')}
      </div>
    )
    let taraweeh
    if (moment().format('iM') === '9') {
      taraweeh = (
        <div className="left">
          {this.state.settings.labels.taraweeh} {this.props.taraweehTime.format('H:mm')}
        </div>
      )
    }
    return (
      <div className="Footer">
        {jummuah}
        {ramadan}
        {taraweeh}
        <div className="center">
          <Offline>
            <img src={wifiOff} className="wifiOff" alt="wifiOff" />
          </Offline>
          <Online>
            <img src={wifiOn} className="wifiOn" alt="wifiOn" />
          </Online>
        </div>
        <div className="right">Refreshed {this.state.refreshed}</div>
        <div className="right">Updated {updated}</div>
      </div>
    )
  }
}

Footer.propTypes = {
  settings: PropTypes.object,
  refreshed: PropTypes.string,
  day: PropTypes.object,
  jummuahTime: PropTypes.object,
  taraweehTime: PropTypes.object,
}

export default Footer
