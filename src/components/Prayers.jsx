import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment-hijri'

import Prayer from '../components/Prayer'

moment.locale('en-ie')

class Prayers extends Component {
  constructor(props) {
    super(props)

    this.state = {
      prayers: { next: { time: moment(), name: '' }, current: { time: moment(), name: '' }, list: [] },
      jamaahShow: true,
      join: 'no',
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.prayers !== this.state.prayers) {
      this.setState({ prayers: nextProps.prayers })
    }
    if (nextProps.jamaahShow !== this.state.jamaahShow) {
      this.setState({ jamaahShow: nextProps.jamaahShow })
    }
    if (nextProps.join !== this.state.join && nextProps.join !== undefined) {
      this.setState({ join: nextProps.join })
    }
  }

  renderPrayers() {
    return (
      <div>
        {this.state.prayers.list.map((prayer, index) => (
          <Prayer
            key={index}
            prayer={prayer}
            nextName={this.state.prayers.next.name}
            jamaahShow={this.state.jamaahShow}
            join={this.state.join}
          />
        ))}
      </div>
    )
  }

  render() {
    let adhan
    let iqamah
    if (this.state.jamaahShow) {
      adhan = <div className="adhanTime">Adhan</div>
      iqamah = <div className="iqamahTime">Iqamah</div>
    } else {
      adhan = <div className="adhanTime right">Adhan</div>
      iqamah = ''
    }

    return (
      <div className="Timetable">
        <div className="prayerHeader">
          <div className="prayerName">Prayer</div>
          {adhan}
          {iqamah}
        </div>
        {this.renderPrayers()}
      </div>
    )
  }
}

export default Prayers

Prayers.propTypes = {
  prayers: PropTypes.object,
  jamaahShow: PropTypes.bool,
  join: PropTypes.string,
}
