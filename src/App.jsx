import React, { Component } from 'react'
// import PropTypes from 'prop-types'

import moment from 'moment-hijri'
// import momenttz from 'moment-timezone'

import { prayersCalc, dayCalc } from 'prayer-timetable-lib'
// import { prayersCalc, dayCalc } from './test_calc' // for testing purposes

import './style/normalize.css'
import './style/App.css'

import Overlay from './components/Overlay'
import Clock from './components/Clock'
import Prayers from './components/Prayers'
import Countdown from './components/Countdown'
import Message from './components/Message'
import Header from './components/Header'
import Footer from './components/Footer'

// moment.tz = require('moment-timezone');
import defsettings from './settings.json'
import deftimetable from './cities/dublin.json'

class TimetableApp extends Component {
  constructor(props) {
    super(props)

    this.state = {
      timetable: deftimetable,
      settings: defsettings,
      day: {},
      prayers: {
        next: { time: moment(), name: '' },
        current: { time: moment(), name: '' },
        list: [],
      },
      tomorrow: 0,
      jamaahShow: true,
      overlayActive: false,
      overlayTitle: ' ... ',
      refresh: 60,
      join: '0',
      log: false,
      refreshed: '-',
      jummuahTime: moment({ hour: defsettings.jummuahtime[0], minute: defsettings.jummuahtime[1] }).day(5),
      taraweehTime: moment({ hour: defsettings.taraweehtime[0], minute: defsettings.taraweehtime[1] }), // .iMonth(8),
    }
  }

  /* *********************************************************************
  STATES
  ********************************************************************* */
  async componentWillMount() {
    document.title = 'ICCI Timetable'
    let newsettings
    let newtimetable
    try {
      if ((await localStorage.getItem('settings')) !== null) {
        newsettings = await JSON.parse(localStorage.getItem('settings'))
        await this.setState({ settings: newsettings })
      }
      if ((await localStorage.getItem('timetable')) !== null) {
        newtimetable = await JSON.parse(localStorage.getItem('timetable'))
        await this.setState({ timetable: newtimetable })
      }
      // await this.setState({ settings: newsettings, timetable: newtimetable, join: newsettings.join })
    } catch (error) {
      console.log(error)
    }

    this.setState({
      // tomorrow=0, settings={jamaahmethods=[], jamaahoffsets=[]}, timetable, jamaahShow='0', join=false, test=false }
      prayers: prayersCalc(
        this.state.tomorrow,
        this.state.settings,
        this.state.timetable,
        this.state.jamaahShow,
        this.state.log
      ),
      day: dayCalc(this.state.tomorrow, {
        hijrioffset: this.state.settings.hijrioffset,
      }),
    })
  }

  async componentDidMount() {
    await this.update()

    this.timerID = setInterval(() => this.tick(), 1000)
    this.updateID = setInterval(() => this.update(), this.state.refresh * 60 * 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
    clearInterval(this.updateID)
  }

  /* *********************************************************************
  SCRIPTS
  ********************************************************************* */
  tick() {
    this.setState({
      prayers: prayersCalc(
        this.state.tomorrow,
        this.state.settings,
        this.state.timetable,
        this.state.jamaahShow,
        this.state.log
      ),
      day: dayCalc(this.state.tomorrow, {
        hijrioffset: this.state.settings.hijrioffset,
      }),
      tomorrow: this.state.prayers.newtomorrow,
    })

    if (moment().isBetween(this.state.jummuahTime, this.state.jummuahTime.clone().add(1, 'hour'))) {
      this.setState({
        overlayActive: true,
        overlayTitle: 'Jummuah Prayer',
      })
    } else if (
      // taraweeh in ramadan first part
      moment().format('iM') === '9' &&
      moment().isBetween(this.state.taraweehTime, this.state.taraweehTime.clone().add(2, 'hour'))
    ) {
      this.setState({
        overlayActive: true,
        overlayTitle: 'Taraweeh Prayer',
      })
    } else if (
      // taraweeh in ramadan second part
      moment().format('iM') === '9' &&
      moment().isBetween(
        moment().startOf('day'),
        moment()
          .startOf('day')
          .clone()
          .add(1, 'hour')
      )
    ) {
      this.setState({
        overlayActive: true,
        overlayTitle: 'Taraweeh Prayer',
      })
    } else {
      this.setState({
        overlayActive: false,
        overlayTitle: ' ... ',
      })
    }
  }

  async update() {
    if (this.state.refresh !== 0) {
      try {
        const resSettings = await fetch('https://islamireland.ie/api/settings/', {
          mode: 'cors',
        })
        // const resTimetable = await fetch('https://islamireland.ie/api/timetable/', {
        //   mode: 'cors',
        // })
        const settings = await resSettings.json()
        // const timetable = await resTimetable.json()
        await this.setState({ settings })
        // await this.setState({ settings, timetable })
        await localStorage.setItem('settings', JSON.stringify(settings))
        // await localStorage.setItem('timetable', JSON.stringify(timetable))

        const jummuahTime = moment({ hour: defsettings.jummuahtime[0], minute: defsettings.jummuahtime[1] }).day(5)
        const taraweehTime = moment({ hour: defsettings.taraweehtime[0], minute: defsettings.taraweehtime[1] })


        await this.setState({ join: settings.join })

        this.setState({
          refreshed: moment().format('HH:mm'),
          jummuahTime,
          taraweehTime,
        })
        console.log('refreshed:', moment().format('HH:mm'))
      } catch (error) {
        console.log(error)
      }
    }
  }

  /* *********************************************************************
  RENDERING
  ********************************************************************* */
  render() {
    // console.log(this.state.overlayActive)
    // console.log(this.state.day)
    // console.log(this.state.jummuahTime)
    // console.log(this.state.taraweehTime)

    let overlay
    if (this.state.overlayActive) {
      overlay = <Overlay settings={this.state.settings} day={this.state.day} overlayTitle={this.state.overlayTitle} />
    } else overlay = ''

    return (
      <div className="TimetableApp">
        {overlay}
        <Header settings={this.state.settings} />
        <Clock day={this.state.day} />
        <Prayers prayers={this.state.prayers} jamaahShow={this.state.jamaahShow} join={this.state.join} />
        <Countdown prayers={this.state.prayers} />
        <Message settings={this.state.settings} />
        <Footer
          settings={this.state.settings}
          day={this.state.day}
          jummuahTime={this.state.jummuahTime}
          taraweehTime={this.state.taraweehTime}
          refreshed={this.state.refreshed}
        />
      </div>
    )
  }
}

export default TimetableApp

// TimetableApp.propTypes = {
//   prayers: PropTypes.any,
//   countdown: PropTypes.string,
// }
