# About

This package is Muslim prayer timetable app for use on displays in mosques. The example mosque is Islamic Cultural Centre of Ireland, but can be adapted for use by any other mosque.
Built using React, using prayer-timetable-lib for calculations.

## Additional info
More details to follow, including instructions, screenshots, etc.